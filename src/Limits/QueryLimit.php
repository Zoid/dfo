<?php

declare(strict_types=1);

namespace Zoid\DFO\Limits;

final class QueryLimit implements IQueryLimit
{
	/** @var int|null */
	private $offset;

	/** @var int|null */
	private $limit;

	public function __construct(int $offset = null, int $limit = null)
	{
		$this->limit = $limit;
		$this->offset = $offset;
	}

	public function getOffset(): ?int
	{
		return $this->offset;
	}

	public function getLimit(): ?int
	{
		return $this->limit;
	}
}