<?php

declare(strict_types=1);

namespace Zoid\Tests\DFO\Tests\Filters\Helpers;

use PHPUnit\Framework\TestCase;
use Zoid\DFO\Filters\ComposedQueryFilter;
use Zoid\DFO\Filters\Helpers\Where;
use Zoid\DFO\Filters\QueryFilter;

final class ComposedFilterTest extends TestCase
{
	public function testValid(): void
	{
		$data = [
			'name' => 'David',
			'text' => 'Something',
			'test' => 2,
		];

		$filter = QueryFilter::fromArray($data);
		$filter
			->and('name')
			->or('text', Where::LIKE)
			->end()
			->and('test', Where::NOT_EQUAL);


		$filter2 = QueryFilter::fromArray(['phone' => 'Samsung']);

		$composed = new ComposedQueryFilter();
		$composed->addFilter($filter)->addFilter($filter2, 'p');

		self::assertSame("(c.name = David OR c.text LIKE Something) AND c.test <> 2 AND p.phone = Samsung", $composed->getStatement('c'));
	}


}
