<?php

declare(strict_types=1);

namespace Zoid\DFO\Filters\Helpers;

use Doctrine\ORM\Query\Expr;
use Zoid\DFO\Filters\Exceptions\InvalidOperationException;

final class Statement
{
	/** @var \Zoid\DFO\Filters\Helpers\Where|null */
	private $expression;

	/** @var Statement|null */
	private $parent;

	/** @var Statement|null */
	private $and;

	/** @var \Zoid\DFO\Filters\Helpers\Statement|null */
	private $or;

	public function __construct(Where $expression, Statement $parent = null)
	{
		$this->expression = $expression;
		$this->parent = $parent;
	}

	public function addAnd(Where $expr) : self
	{
		return $this->and = new self($expr, $this);
	}

	public function addOr(Where $expr) : self
	{
		return $this->or = new self($expr, $this);
	}

	public function endStatement() : self
	{
		if($this->or !== null) {
			return $this;
		}

		if($this->parent === null) {
			throw InvalidOperationException::noNestedQueries();
		}

		return $this->parent->endStatement();
	}

	public function mergeExpressions(string $prefix) : string
	{
		$expr = new Expr();
		$expression = $this->expression->toString($prefix);

		if($this->and === null && $this->or === null) {
			return $this->expression->toString($prefix);
		}

		if($this->or !== null) {
			$expression = (string)$expr->orX(
				$expression,
				$this->or->mergeExpressions($prefix)
			);
		}

		if($this->and !== null) {
			$expression = (string)$expr->andX(
				$expression,
				$this->and->mergeExpressions($prefix)
			);
		}

		return $expression;
	}
}