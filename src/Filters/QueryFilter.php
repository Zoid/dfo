<?php

declare(strict_types=1);

namespace Zoid\DFO\Filters;

use Zoid\DFO\Filters\Exceptions\InvalidOperationException;
use Zoid\DFO\Filters\Helpers\Statement;
use Zoid\DFO\Filters\Helpers\Where;

final class QueryFilter implements IQueryFilter
{
	/** @var Statement|null */
	private $root;

	/** @var \Zoid\DFO\Filters\Helpers\Statement|null */
	private $active;

	/** @var array */
	private $data;

	/**
	 * QueryFilter constructor.
	 * @param mixed[] $data
	 */
	private function __construct(array $data)
	{
		$this->data = $data;
	}

	/**
	 * @param mixed[] $data
	 * @return \Zoid\DFO\Filters\QueryFilter|null
	 */
	public static function fromArray(array $data) :? self
	{
		if(\count($data) === 0) {
			return null;
		}

		return new self($data);
	}

	public function and(string $key, string $expr = Where::EQUAL) : self
	{
		if(!array_key_exists($key, $this->data)) {
			return $this;
		}

		$expr = new Where($key, $expr, $this->data[$key]);

		if($this->active === null) {
			$this->active = $this->root = new Statement($expr);
		} else {
			$this->active = $this->active->addAnd($expr);
		}

		return $this;
	}

	public function or(string $key, string $expr = Where::EQUAL) : self
	{
		if(!array_key_exists($key, $this->data)) {
			return $this;
		}

		$expr = new Where($key, $expr, $this->data[$key]);

		if($this->active === null) {
			$this->active = $this->root = new Statement($expr);
		} else {
			$this->active = $this->active->addOr($expr);
		}

		return $this;
	}

	public function end() : self
	{
		if($this->active === null) {
			throw InvalidOperationException::noNestedQueries();
		}

		$this->active = $this->active->endStatement();
		return $this;
	}

	public function getStatement(string $prefix) : string
	{
		if($this->root === null)
		{
			foreach ($this->data as $key => $val) {
				$this->and($key);
			}

			if($this->root === null) {
				return '';
			}
		}

		return $this->root->mergeExpressions($prefix);
	}
}