<?php

declare(strict_types=1);

namespace Zoid\DFO\Filters\Helpers;

use Doctrine\ORM\Query\Expr;

final class Where
{
	const EQUAL = 'eq';
	const NOT_EQUAL = 'neq';
	const LIKE = 'like';
	const IS_NULL = 'isNull';
	const IS_NOT_NULL = 'isNotNull';
	const GREATER_THEN = 'gt';
	const GREATER_THEN_EQUAL = 'gte';
	const LESS_THEN = 'lt';
	const LESS_THEN_EQUAL = 'lte';
	const IN = 'in';

	/** @var string */
	private $columnName;

	/** @var string */
	private $expr;

	/** @var mixed */
	private $value;

	/**
	 * Expression constructor.
	 * @param string $columnName
	 * @param string $expr
	 * @param mixed  $value
	 */
	public function __construct(string $columnName, string $expr, $value)
	{
		$this->columnName = $columnName;
		$this->expr = $expr;
		$this->value = $value;
	}


	public function toString(string $prefix) : string
	{
		$builder = new Expr();

		switch ($this->expr)
		{
			case self::LIKE:
				return (string)$builder->like($prefix . "." . $this->columnName, $this->value);
			case self::EQUAL:
				return (string)$builder->eq($prefix . "." . $this->columnName, $this->value);
			case self::NOT_EQUAL:
				return (string)$builder->neq($prefix . "." . $this->columnName, $this->value);
			case self::IS_NULL:
				return (string)$builder->isNull($prefix . "." . $this->columnName);
			case self::IS_NOT_NULL:
				return (string)$builder->isNotNull($prefix . "." . $this->columnName);
			case self::GREATER_THEN:
				return (string)$builder->gt($prefix . "." . $this->columnName, $this->value);
			case self::GREATER_THEN_EQUAL:
				return (string)$builder->gte($prefix . "." . $this->columnName, $this->value);
			case self::LESS_THEN:
				return (string)$builder->lt($prefix . "." . $this->columnName, $this->value);
			case self::LESS_THEN_EQUAL:
				return (string)$builder->lte($prefix . "." . $this->columnName, $this->value);
			case self::IN:
				return (string)$builder->in($prefix . "." . $this->columnName, $this->value);

			default:
				throw new \InvalidArgumentException("Unknown expression '{$this->expr}'");
		}
	}
}