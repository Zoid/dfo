<?php

declare(strict_types=1);

namespace Zoid\Tests\DFO\Tests\Filters\Helpers;

use PHPUnit\Framework\TestCase;
use Zoid\DFO\Filters\Helpers\Where;

final class StringStreamTest extends TestCase
{
	public function testValid(): void
	{
		$where1 = new Where('name', Where::EQUAL, 'David');
		self::assertSame("c.name = David", $where1->toString('c'));

		$where2 = new Where('text', Where::LIKE, 'Someth');
		self::assertSame("c.text LIKE Someth", $where2->toString('c'));
	}

	public function testThrowing(): void
	{
		self::expectException(\LogicException::class);
		(new Where('name', 'unexistingExpression', null))->toString('c');
	}
}
