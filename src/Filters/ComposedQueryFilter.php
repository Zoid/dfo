<?php

declare(strict_types=1);

namespace Zoid\DFO\Filters;

use Zoid\DFO\Filters\Dto\MappedFilter;
use Doctrine\ORM\Query\Expr;

final class ComposedQueryFilter implements IQueryFilter
{
	/** @var \Zoid\DFO\Filters\Dto\MappedFilter[] */
	private $filters = [];

    public function __construct(array $filters)
    {
        $this->filters = array_map(static function(IQueryFilter $filter) : MappedFilter {
            return new MappedFilter($filter,null);
        }, $filters);
    }

    public function addFilter(IQueryFilter $filter, string $prefix = null) : self
	{
		$this->filters[] = new MappedFilter($filter, $prefix);
		return $this;
	}

	public function getStatement(string $prefix) : string
	{
		$statements = array_map(static function(MappedFilter $mappedFilter) use($prefix) : string {
			return $mappedFilter->getFilter()->getStatement($mappedFilter->getPrefix() ?? $prefix);
		}, $this->filters);

		$statements = \array_filter($statements);

		if(count($statements) === 0) {
			return '';
		}

		return (string)call_user_func_array([new Expr(), 'andX'], $statements);
	}
}