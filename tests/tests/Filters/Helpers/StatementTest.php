<?php

declare(strict_types=1);

namespace Zoid\Tests\DFO\Tests\Filters\Helpers;

use PHPUnit\Framework\TestCase;
use Zoid\DFO\Filters\Exceptions\InvalidOperationException;
use Zoid\DFO\Filters\Helpers\Statement;
use Zoid\DFO\Filters\Helpers\Where;

final class StatementTest extends TestCase
{
	public function testValid(): void
	{
		$where1 = new Where('name', Where::EQUAL, 'David');
		$where2 = new Where('text', Where::LIKE, 'Something');
		$where3 = new Where('test', Where::EQUAL, 2);

		$statement = new Statement($where1);
		$x = $statement->addAnd($where2);
		self::assertSame("c.name = David AND c.text LIKE Something", $statement->mergeExpressions('c'));

		$x->addOr($where3);

		self::assertSame("c.name = David AND (c.text LIKE Something OR c.test = 2)", $statement->mergeExpressions('c'));


		$statement = new Statement($where1);
		$statement->addOr($where2)->endStatement()->addAnd($where3);

		self::assertSame("(c.name = David OR c.text LIKE Something) AND c.test = 2", $statement->mergeExpressions('c'));

	}

	public function testThrowing() : void
	{
		$where1 = new Where('name', Where::EQUAL, 'David');

		self::expectException(InvalidOperationException::class);
		$statement = new Statement($where1);
		$statement->endStatement();
	}

}
