<?php

declare(strict_types=1);

namespace Zoid\DFO\Filters\Dto;

final class MappedFilter
{
	/** @var \Zoid\DFO\Filters\IQueryFilter */
	private $filter;

	/** @var string|null */
	private $prefix;

	/**
	 * MappedFilter constructor.
	 * @param \Zoid\DFO\Filters\IQueryFilter $filter
	 * @param string|null                    $prefix
	 */
	public function __construct(\Zoid\DFO\Filters\IQueryFilter $filter, ?string $prefix)
	{
		$this->filter = $filter;
		$this->prefix = $prefix;
	}

	public function getFilter(): \Zoid\DFO\Filters\IQueryFilter
	{
		return $this->filter;
	}

	public function getPrefix(): ?string
	{
		return $this->prefix;
	}
}
