<?php

declare(strict_types=1);

namespace Zoid\DFO\Sorting;

final class QueryOrder implements IQueryOrder
{
	/** @var string[] */
	private $order;

	public function __construct(array $order)
	{
		$this->order = $order;
	}

	public function getColumnName(string $prefix) : string
	{
		return $prefix . "." . $this->order[0];
	}

	public function getOrder() : string
	{
		return $this->order[1];
	}
}