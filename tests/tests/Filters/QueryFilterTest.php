<?php

declare(strict_types=1);

namespace Zoid\Tests\DFO\Tests\Filters\Helpers;

use PHPUnit\Framework\TestCase;
use Zoid\DFO\Filters\Exceptions\InvalidOperationException;
use Zoid\DFO\Filters\Helpers\Where;
use Zoid\DFO\Filters\QueryFilter;

final class QueryFilterTest extends TestCase
{
	public function testValid(): void
	{
		$data = [
			'name' => 'David',
			'text' => 'Something',
			'test' => 2,
		];

		$filter = QueryFilter::fromArray($data);
		$filter
			->and('name')
				->or('text', Where::LIKE)
				->end()
			->and('test', Where::NOT_EQUAL);

		self::assertSame("(c.name = David OR c.text LIKE Something) AND c.test <> 2", $filter->getStatement('c'));


		$filter = QueryFilter::fromArray([]);
		self::assertNull($filter);

		$filter = QueryFilter::fromArray(['name' => 'David', 'phone' => 'Samsung']);
		self::assertSame("c.name = David AND c.phone = Samsung", $filter->getStatement('c'));

	}

	public function testValid2() : void
	{
		$data = [
			'name' => 'David',
			'location' => 'USA',
			'age' => 26,
			'position' => 'Manager'
		];

		$filter = QueryFilter::fromArray($data);

		$filter ->and('name')
			->and('location')
			->or('age', Where::NOT_EQUAL)
			->end()
			->and('position', Where::LIKE);

		self::assertSame("c.name = David AND ((c.location = USA OR c.age <> 26) AND c.position LIKE Manager)", $filter->getStatement('c'));
	}

	public function testThrowing() : void
	{
		self::expectException(InvalidOperationException::class);
		$filter = QueryFilter::fromArray(['name' => 'David']);
		$filter->end();

		self::expectException(InvalidOperationException::class);
		$filter = QueryFilter::fromArray(['name' => 'David']);
		$filter->and('name')->end();
	}

}
