<?php

declare(strict_types=1);

namespace Zoid\DFO\Sorting;

interface IQueryOrder
{
	public function getColumnName(string $prefix) : string;

	public function getOrder() : string;
}