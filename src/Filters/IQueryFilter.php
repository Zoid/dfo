<?php

declare(strict_types=1);

namespace Zoid\DFO\Filters;

interface IQueryFilter
{
	public function getStatement(string $prefix) : string;
}