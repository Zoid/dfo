<?php

declare(strict_types=1);

namespace Zoid\DFO\Limits;

interface IQueryLimit
{
	public function getOffset() : ?int;

	public function getLimit() : ?int;
}