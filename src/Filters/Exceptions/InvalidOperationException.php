<?php

declare(strict_types=1);

namespace Zoid\DFO\Filters\Exceptions;

final class InvalidOperationException extends \LogicException
{
	public static function noNestedQueries() : self
	{
		return new self("You can not call ->end() in this context, there are conditions active");
	}
}