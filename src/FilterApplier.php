<?php

declare(strict_types=1);

namespace Zoid\DFO;

use Doctrine\ORM\QueryBuilder;
use Zoid\DFO\Filters\IQueryFilter;
use Zoid\DFO\Limits\IQueryLimit;
use Zoid\DFO\Sorting\IQueryOrder;

final class FilterApplier
{
	/** @var QueryBuilder */
	private $queryBuilder;

	public function __construct(QueryBuilder& $builder)
	{
		$this->queryBuilder = $builder;
	}

	public function filter(IQueryFilter $filter, string $overridePrefix = null) : self
	{
		$prefix = $overridePrefix ?? current($this->queryBuilder->getRootAliases());
		$this->queryBuilder->andWhere($filter->getStatement($prefix));

		return $this;
	}

	public function order(IQueryOrder $order, string $overridePrefix = null) : self
	{
		$prefix = $overridePrefix ?? current($this->queryBuilder->getRootAliases());
		$this->queryBuilder->addOrderBy($order->getColumnName($prefix), $order->getOrder());

		return $this;
	}

	public function limit(IQueryLimit $limit) : void
	{
		if($limit->getOffset() !== null) {
			$this->queryBuilder->setFirstResult($limit->getOffset());
		}

		if($limit->getLimit() !== null) {
			$this->queryBuilder->setMaxResults($limit->getLimit());
		}
	}
}